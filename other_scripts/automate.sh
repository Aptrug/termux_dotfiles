#!/bin/sh

#if ! /system/bin/pgrep -x com.termux; then
# start the flow that backups Automate. Do not modify the flow in any
# way, otherwise you will have to change the following line, again
/system/bin/am start -a com.llamalab.automate.intent.action.START_FLOW \
	-d content://com.llamalab.automate.provider/flows/19/statements/1

# HomeActivity is the way to go
/system/bin/am start -n com.termux/com.termux.HomeActivity
# sleep is needed, otherwise Termux will not initialise the shell
sleep 1
# the following line simulates HOME button being pressed
/system/bin/am start -a android.intent.action.MAIN -c android.intent.category.HOME
#fi
