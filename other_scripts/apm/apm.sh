#!/bin/sh

set -efu
export POSIXLY_CORRECT=1

query="$(
	eval "$(
		awk '
BEGIN {
	printf "curl --silent --user-agent \"%s\" ",
	"Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"
	FS = "/"
}

/^https/{
	printf "'\''"
	for (i = 1; i < 6; ++i) {
		printf "%s/", $i
	}
	printf "%s/'\'' ", $6
}

END {
	printf "\n"
}
' ./packagesList
	)"
)"

appNver="$(
	awk '
BEGIN {
	k = 0
}

{

	if (/^<h3 title/) {
		sub(/^<h3 title="/, "", $0)
		sub(/".*/, "", $0)
		n = split($0, ar, " ")
	} else if (/^<a target="_blank" t/) {
		sub(/.*details\?id=/, "", $0)
		sub(/".*/, "", $0)
		printf "[%s] [", $0
		# Some times a random space appears,
		# we need to take it into account
	} else if (/^[[:space:]]?<a class="twitter/) {
		sub(/.*Just downloaded the /, "", $0)
		sub(/ APK from.*/, "", $0)
		m = split($0 ,arr, " ")
		for (i = n + 1; i < m; ++i) {
			printf "%s ", arr[i]
		}
		printf "%s] [%s]\n", arr[m], ++k
	}
}
' <<-EOF
		${query}
	EOF
)"

# echo "${appNver}"
# exit

toBeDownloaded="$(
	awk '
BEGIN {
	FS = "[][]"
	k = 0
}
{
	if (FILENAME == "/dev/fd/3") {
		packages[$2] = $4
		number[$2] = $6
	} else if (FILENAME == "/dev/fd/4") {
		if ($0 ~ /^  P/ && $2 in packages) {
			app = $2
			# I know what I am doing
			while ($0 !~ /^    versionName/) {
				getline
			}
			sub(/.*[^=]+=/, "", $0)
			if (packages[app] == $0) {
				delete packages[app]
			}
		}
	} else if ($0 ~ /^https/) {
		++k
		for (i in packages) {
			if (k == number[i]) {
				print $0
			}
		}
	}
}

' /dev/fd/3 /dev/fd/4 ./packagesList 3<<-EOF 4<<-EOF
		${appNver}
	EOF
		$(su -c 'dumpsys package packages')
	EOF
)"

query="$(
	eval "$(
		awk '
BEGIN {
    printf "curl --silent --user-agent \"%s\"",
	"Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"
}

{
	printf " '\''%s'\''", $0
}

END {
    printf "\n"
}
' <<-EOF
			${toBeDownloaded}
		EOF
	)"
)"

closer="$(
	awk '
BEGIN {
	FS = "/"
	k = 1
}

{
	if (/location\.href/) {
		k = 1
	} else if (k && /^<a class="downloadLink"/) {
		sub(/.* href="/, "", $0)
		sub(/".*/, "", $0)
		print "https://www.apkmirror.com" $0
		k = 0
	}
}
' <<-EOF
		${query}
	EOF
)"

exceptions="$(
	eval "$(
		awk '
BEGIN {
	k = 0
	ORS = " "
}
(!(/android-apk-download\/$/)) {
	if (++k == 1) {
		if($0){
			print "curl --parallel --silent --location --user-agent \"Mozilla/5.0",
			"(X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0\"",
			"--write-out \"%{url_effective}\n\" --output /dev/null",
			$0
		} else {
			exit
		}
	} else {
		print "--output /dev/null", $0
	}
}

END {
	printf "\n"
}
' <<-EOF
			${closer}
		EOF
	)"
)"

query="$(
	eval "$(
		awk '
BEGIN {
	k = 0
}

/android-apk-download/ {
	if (++k == 1) {
		if ($0) {
			# I can run parallel here, no problem
			printf "curl --parallel --silent --user-agent \"%s\"",
			"Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"
		} else {
			exit
		}
	}
	printf " %sdownload/", $0
}

END {
	printf "\n"
}
' /dev/fd/3 /dev/fd/4 3<<-EOF 4<<-EOF
			${closer}
		EOF
			${exceptions}
		EOF
	)"
)"

# echo "$closer"
# exit
# echo "$exceptions"
# exit

# eval "$(
awk '
BEGIN {
	FS = "\""
	k = 0
	ORS = " "
	print "curl --location --user-agent \"Mozilla/5.0",
	"(X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0\"",
	"--parallel --continue-at - --create-dirs"
}

/download\.php\?/ {
	print "--output",
	"/data/data/com.termux/files/usr/tmp/apm/" ++k ".apk",
	"https://www.apkmirror.com" $(NF - 1)
}

END {
	printf "\n"
}
' <<-EOF
	${query}
EOF
# )"

# printf "\n\n%s\n", "${closer}"
