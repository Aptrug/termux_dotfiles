#!/bin/sh

appops set com.catchingnow.tinyclipboardmanager SYSTEM_ALERT_WINDOW allow;
pm grant com.catchingnow.tinyclipboardmanager android.permission.READ_LOGS;
am force-stop com.catchingnow.tinyclipboardmanager;
