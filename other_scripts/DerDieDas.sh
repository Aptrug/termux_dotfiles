#!/bin/sh

su -c "sqlite3 -separator ' ' /data/data/com.lubosmikusiak.articuli.derdiedas/databases/nouns.sqlite '
select article_mask, word from noun_2;
	'" | sed '
s/1/der/
s/2/die/
s/3/der, die/
s/4/das/
s/5/der, das/
s/6/die, das/
s/7/der, die, das/'
#| fzf --nth="2" --exact --preview '
#curl --silent dict://dict.org/d:{-1}:fd-deu-eng |
#	grep --invert-match "^[0-9]"
#'
