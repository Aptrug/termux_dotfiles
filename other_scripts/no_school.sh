#!/bin/bash

# turning off WLAN
termux-wifi-enable true

# no sound
termux-volume alarm 25
termux-volume music 25
termux-volume notification 25
termux-volume ring 25
termux-volume system 25
termux-volume call 25
