#!/bin/sh

su -c "sqlite3 -separator ', ' /data/data/com.lubosmikusiak.articuli.lela/databases/nouns.sqlite '
select word, article_mask from noun_2;
	'" | sed '
s/\(1\|4\)$/masculin/
s/\(2\|8\)$/féminin/
s/\(3\|12\)$/épicène/'
#	| fzf --nth="1" --exact --preview '
#curl --silent dict://dict.org/d:{1}:fd-fra-eng |
#	grep --invert-match "^[0-9]"
#'
