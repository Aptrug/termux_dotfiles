#!/bin/sh

# yadm() {
# 	/data/data/com.termux/files/home/scripts/fajita/yadm "$@"
# }

yadm add /data/data/com.termux/files/home/.config/ \
	/data/data/com.termux/files/home/.termux/ \
	/data/data/com.termux/files/home/.ssh/ \
	/data/data/com.termux/files/home/bin/ \
	/data/data/com.termux/files/home/scripts/fajita/ \
	/data/data/com.termux/files/home/other_scripts/ \
	/data/data/com.termux/files/home/misc/ \
	/data/data/com.termux/files/home/.profile

# remove non-existing files from the git repo
while read -r path; do
	absolutePath="/data/data/com.termux/files/home/${path}"
	if ! [ -f "${absolutePath}" ] && ! [ -d "${absolutePath}" ]; then
		yadm rm -rf --quiet "${absolutePath}"
	fi
done <<-EOF
	$(yadm ls-tree --full-tree -r --name-only HEAD)
EOF

# push the changes with an empty commit message
yadm commit --all --message='‏‏‎'
yadm push --set-upstream origin master
