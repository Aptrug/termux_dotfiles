#!/data/data/com.termux/files/usr/bin/bash

exit_if_already_running() {
	local ps_command
	ps_command="$(ps -eo command=)"
	awk '
	BEGIN {
		instance = 0
	}
	{
		if (NF == 2 && $2 == "'"$0"'") {
			if (++instance == 2) {
				exit 1
			}
		}
	}
	' <<-EOF || exit
		${ps_command}
	EOF
}

show_netspeed() {
	local rx
	local tx
	local rx_plus_tx
	local session_usage
	local netspeed
	local netspeed_file
	netspeed_file="/data/data/com.termux/files/home/misc/gatherer/netspeed.txt"
	rx="$(/system/bin/su -c '
	/system/bin/cat /sys/class/net/wlan0/statistics/rx_bytes
	')"
	tx="$(/system/bin/su -c '
	/system/bin/cat /sys/class/net/wlan0/statistics/tx_bytes
	')"

	rx_plus_tx=$((rx + tx))
	netspeed=$(((rx_plus_tx - rx_plus_tx_old) / 1024))
	session_usage=$((rx_plus_tx / 1048576))
	rx_plus_tx_old=${rx_plus_tx}
	printf "%sM | %sk\n" "${session_usage}" "${netspeed}" >"${netspeed_file}"
}

show_battery() {
	local status
	local capacity
	local battery_file
	battery_file="/data/data/com.termux/files/home/misc/gatherer/battery.txt"
	status="$(/system/bin/su -c '
	/system/bin/cat /sys/class/power_supply/battery/status
	')"
	capacity="$(/system/bin/su -c '
	/system/bin/cat /sys/class/power_supply/battery/capacity
	')"
	if [ "${status}" = "Charging" ]; then
		status="+"
	else
		status="-"
	fi
	printf "%s%s%%\n" "${status}" "${capacity}" >"${battery_file}"
}

main() {
	exit_if_already_running
	local counter
	rx_plus_tx_old=0
	counter=300
	while ! read -r -t 1 -u 1; do
		if [ "${counter}" = 300 ]; then
			show_battery
			counter=0
		fi
		show_netspeed
		counter=$((counter + 1))
	done
}

main
