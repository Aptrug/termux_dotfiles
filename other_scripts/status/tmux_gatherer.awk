#!/data/data/com.termux/files/usr/bin/gawk -f

@load "time"

BEGIN {

	while(1) {
		netspeed_file = "/data/data/com.termux/files/home/misc/gatherer/netspeed.txt"
		battery_file = "/data/data/com.termux/files/home/misc/gatherer/battery.txt"
		if ((getline netspeed < netspeed_file) > 0) {
			close(netspeed_file)
		}
		if ((getline battery < battery_file) > 0) {
			close(battery_file)
		}
		printf "%s | %s\r", netspeed, battery
		sleep(1)
	}
}
