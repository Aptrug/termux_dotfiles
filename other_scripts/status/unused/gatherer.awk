#!/data/data/com.termux/files/usr/bin/gawk -f

@load "time"

function exit_if_already_running(instances, command, ps_command)
{
	instances = 0
	command = sprintf("su -c %s",
		"/data/data/com.termux/files/home/other_scripts/status/gatherer.awk")
	ps_command = "ps -eo args"
	while ((ps_command | getline) > 0) {
		if ($0 == command && ++instances == 1) {
			close(command)
			return 1
		}
	}
	close(command)
	return 0
}


function show_battery(uevent,
					  battery_status,
					  status,
					  capacity,
					  battery_level,
					  battery_file)
{
	battery_file = "/data/data/com.termux/files/home/misc/gatherer/battery_status.txt"
	uevent = "/sys/class/power_supply/battery/uevent"
	while ((getline < uevent) > 0) {
		if (sub(/^POWER_SUPPLY_STATUS=/, "", $0)) {
			if ($0 == "Charging") {
				status = "+"
			} else {
				status = "-"
			}
		} else if (sub(/^POWER_SUPPLY_CAPACITY=/, "", $0)) {
			capacity = $0
			break
		}
	}
	close(uevent)
	battery_level = status capacity "%"
	printf "%s\n", battery_level > battery_file
	close(battery_file)
}

function get_rx_plus_tx_old(rx, rx_bytes,
							tx, tx_bytes)
{
	rx_bytes = "/sys/class/net/wlan0/statistics/rx_bytes"
	tx_bytes = "/sys/class/net/wlan0/statistics/tx_bytes"
	if ((getline rx < rx_bytes) > 0) {
		close(rx_bytes)
	}
	if ((getline tx < tx_bytes) > 0) {
		close(tx_bytes)
	}
	rx_plus_tx_old = rx + tx
	if (rx_plus_tx_old != 0) {
		return 1
	} else {
		return 0
	}
}

function show_netspeed(rx, rx_bytes,
						tx, tx_bytes,
						rx_plus_tx,
						netspeed,
						session_usage,
						netspeed_file)
{
	netspeed_file = "/data/data/com.termux/files/home/misc/gatherer/netspeed.txt"
	rx_bytes = "/sys/class/net/wlan0/statistics/rx_bytes"
	tx_bytes = "/sys/class/net/wlan0/statistics/tx_bytes"
	if ((getline rx < rx_bytes) > 0) {
		close(rx_bytes)
	}
	if ((getline tx < tx_bytes) > 0) {
		close(tx_bytes)
	}
	rx_plus_tx = rx + tx
	netspeed = (rx_plus_tx - rx_plus_tx_old) / 1024
	session_usage = rx_plus_tx / 1048576
	rx_plus_tx_old = rx_plus_tx
	printf "%dM | %dk\n", session_usage, netspeed > netspeed_file
	# The file must be closed otherwise it will not be overwritten
	# the next time.
	close(netspeed_file)
}


BEGIN {
	# if (exit_if_already_running()) {
	# 	exit
	# }
	LINT = 1
	if (get_rx_plus_tx_old())
		exit
	}
	counter = 299
	# sleep() always returns 0
	while (!(sleep(1))) {
		if (++counter == 300) {
			counter = 0
			show_battery()
		}
		show_netspeed()
		# Will not output anything to files without fflush()
		fflush()
	}
}
