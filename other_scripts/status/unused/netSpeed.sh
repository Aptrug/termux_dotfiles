#!/bin/sh

outputFile=/data/data/com.termux/files/home/misc/netSpeed.txt
previousRxPlusTx=0
while true; do
	Rx="$(su -c 'cat /sys/class/net/wlan0/statistics/rx_bytes')"
	Tx="$(su -c 'cat /sys/class/net/wlan0/statistics/tx_bytes')"
	RxPlusTx=$((Rx + Tx))
	TNU=$((RxPlusTx / 1048576))
	netSpeed=$(((RxPlusTx - previousRxPlusTx) / 1024))
	printf "TNU: %s M, NS: %s KB/s\r" "${TNU}" "${netSpeed}" >"${outputFile}"
	previousRxPlusTx=${RxPlusTx}
	sleep 1
done
