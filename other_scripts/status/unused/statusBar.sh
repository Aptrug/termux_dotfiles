#!/bin/sh

while true; do
	termux-notification --priority min --id "battery_status" --ongoing \
		--content "$(cat /data/data/com.termux/files/home/misc/battery_status.txt)"
	sleep 300
	termux-notification-remove "battery_status"
done
