#!/bin/sh

outputFile=/data/data/com.termux/files/home/misc/battery_status.txt
while true; do
	su -c 'dumpsys battery' | awk '
{
	if (NR == 8) {
		status = $2
	} else if (NR == 11) {
		if (status == 3) {
			print "-" $2 "%"
		} else {
			print "+" $2 "%"
		}
		exit
	}
}
	' >"${outputFile}"

	sleep 300
done
