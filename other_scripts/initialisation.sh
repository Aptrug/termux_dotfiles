#!/bin/sh

# awk '
# 	BEGIN {
# 		instance = 0
# 	}
#
# 	{
# 		if ($1 == "/data/data/com.termux/files/usr/bin/gawk" &&
# 			++instance == 2) {
# 			exit 1
# 		}
# 	}
# ' <<EOF || exit
# $(ps -eo command=)
# EOF

pgrep sshd >/dev/null && exit

# important ones first
#termux-wake-lock

/data/data/com.termux/files/usr/bin/sshd &

# /data/data/com.termux/files/usr/bin/aria2c &

/data/data/com.termux/files/home/other_scripts/gitPusher.sh &

# /data/data/com.termux/files/home/other_scripts/status/gatherer.sh &

# /data/data/com.termux/files/home/other_scripts/status/battery_status.sh &
#
# /data/data/com.termux/files/home/other_scripts/status/netSpeed.sh &

#emacs --daemon
/data/data/com.termux/files/usr/bin/apt-get update &&
	/data/data/com.termux/files/usr/bin/apt-get autoremove -y &&
	/data/data/com.termux/files/usr/bin/apt-get upgrade -y
#/data/data/com.termux/files/usr/bin/gem update

# backup automate flows
# I set automate alarmClock to do this instead
#su -c 'content read --uri content://com.llamalab.automate.provider/backup \
# 	>/data/data/com.termux/files/home/.local/share/automate/automate.bak'
