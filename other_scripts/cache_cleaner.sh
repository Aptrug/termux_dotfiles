#!/bin/sh

# NOTE without `! -empty` `rm` will later complain about empty directories
cache="$(su -c 'find /data/data/ -maxdepth 2 -type d -name cache ! -empty')"
[ -z "${cache}" ] && exit

# List the cache directories whose contents could be removed along with
# their corresponding sizes
eval "$(
	awk '
BEGIN {
	printf "su -c '\''du -chs"
}

{
	printf " %s", $0
}

END {
	printf "'\''\n"
}
' <<-EOF
		${cache}
	EOF
)"

printf "\nClear the contents of the above directories (n/y): "
read -r choice
if [ "${choice}" != "y" ]; then
	exit
fi

eval "$(
	awk '
BEGIN {
	printf "su -c '\''rm -fr"
}

{
	printf " %s/*", $0
}

END {
	printf "'\''\n"
}
' <<-EOF
		${cache}
	EOF
)"
