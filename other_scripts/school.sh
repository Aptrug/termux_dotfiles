#!/bin/bash

# turning off WLAN
termux-wifi-enable false

# no sound
termux-volume alarm 0
termux-volume music 0
termux-volume notification 0
termux-volume ring 0
termux-volume system 0
termux-volume call 0
