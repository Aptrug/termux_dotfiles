#!/bin/sh

# run the whole script as su, it is safe

something="$(/system/bin/cmd settings get system pointer_location)"
if [ "${something}" = '0' ]; then
	/system/bin/settings put system pointer_location 1
	/system/bin/echo enabled
else
	/system/bin/settings put system pointer_location 0
	/system/bin/echo disabled
fi
