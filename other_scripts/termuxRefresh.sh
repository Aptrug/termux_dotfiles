#!/bin/sh

# turn on the screen (a must)
/system/bin/input keyevent KEYCODE_POWER
termuxPID="$(/system/bin/pgrep -x com.termux)"

if [ -n "${termuxPID}" ]; then
	/system/bin/kill "${termuxPID}"
	/system/bin/sleep 0.1
fi

/system/bin/am start -n com.termux/com.termux.HomeActivity
# sleep is needed, otherwise Termux will not initialise the shell
/system/bin/sleep 0.1
# the following line simulates HOME button being pressed
#/system/bin/am start -a android.intent.action.MAIN -c android.intent.category.HOME

# press recent key
/system/bin/input keyevent KEYCODE_APP_SWITCH

# swipe to remove termux from  APP_SWITCH (x1 y1 x2 y2 t)
/system/bin/input swipe 530 1570 530 340 100

# turn off the screen
/system/bin/input keyevent KEYCODE_POWER
