#!/bin/sh

# some shell stuff{{{1

# posix ftw
export POSIXLY_CORRECT=1

# better avoid any shell quirks
set -efu
# exit if already running{{{1

# exit once more than one instance of this
# shell script is found in order to avoid
# any conflicts
awk '
	BEGIN {
		instance = 0
	}

	/^sh .*\/alarmClock.sh$/ {
		if (++instance == 2) {
			exit 1
		}
	}
' <<EOF || exit
$(ps -eo command=)
EOF
# variables to set {{{1

# your favorite ringtone
ringtone=/storage/emulated/0/Music/A_Call_to_Battle.m4a

# choose an arbitrary number; and make sure you are
# not using it in any of your personal scripts
# otherwise there will be a conflict between
# the notifications
notificationId=3356

# for testing purposes
# ringtone=/storage/emulated/0/Music/Windows.mp3
# cleanup function{{{1

# exiting the script will trigger the following function
cleanup() {

	# stop any while loops running in the background
	[ -n "${loopPid-}" ] && [ -d /proc/"${loopPid}"/ ] &&
		kill "${loopPid}"

	# stop all running instances of these two commands
	termux-notification-remove "${notificationId}"
	termux-media-player stop >/dev/null
	exit
}
trap cleanup INT TERM EXIT
# get the alarm time{{{1

# we will be using `termux-dialog`
# to get the alarm time
# NSFW WARNING: it only supports
# the dreaded AM/PM format that
# 96 % of the world population do
# not use
alarmTime="$(
	termux-dialog time \
		-t 'set the alarm time' |
		grep --only-matching \
			'[0-9]\{2\}:[0-9]\{2\}' ||
		# the script will exit if no time was provided
		exit
)"
# Ringtone length {{{1

eval "$(
	termux-volume | awk '
		BEGIN {
			FS = "[ ,]+"
		}

		/"music",$/{
			getline
			print "volume=" $(NF - 1),
			"\ntermux-volume music 0"
			exit
		}
	'
)"

termux-media-player play "${ringtone}" >/dev/null

eval "$(
	awk '
		BEGIN {
			FS = "[ :]+"
		}
		(NR == 3){
			seconds = 0
			k = 1
			for (i = NF; i > 0; --i) {
				seconds += $i * k
				k *= 60
			}
			printf "ringtonDuration=%s\n", seconds
			exit
		}
		' <<-EOF
		$(termux-media-player info)
	EOF
)"
termux-media-player stop >/dev/null &
termux-volume music "${volume-}"
# seconds till the alarm starts{{{1

# this method works even if the alarm time is
# not on the same day as the current time
secondsLeft() {
	awk \
		-v now_secondsSinceMidnight="$(($(date +%s) - \
		$(date --date='00:00:00' +%s)))" '
		BEGIN {
			FS = ":"
		}

		{
			alarmTime_secondsSinceMidnight = $1 * 3600 + $2 * 60
			d = alarmTime_secondsSinceMidnight - now_secondsSinceMidnight
			print (86400 + d) % 86400
		}
' <<-EOF
			${alarmTime}
		EOF
}
# convert the time left from seconds to HH:MM{{{1

# it will be displayed later in the notification
# along with the alarm time
timeLeft() {
	awk '
		{
			hours = $0 / 3600
			minutes = (hours % 1) * 60
			printf "%02d:%02d\n", hours, minutes
		}
' <<-EOF
		$(secondsLeft)
	EOF
}
# display the notification and wait for the alarm time{{{1

# the notification will keep showing until the last
# minute of the alarm time

# we can use timeLeft to detect the alarm time
# [ $(timeLeft) != '00:00' ], but we will use
# secondsLeft since it is an integer
# and integer comparison is always
# more efficient than string comparison
while [ "$(secondsLeft)" -gt 60 ]; do
	termux-notification \
		--id="${notificationId}" \
		--ongoing \
		--icon alarm \
		--button1 "abort" --button1-action "
			termux-notification-remove ${notificationId}
			termux-media-player stop >/dev/null

			# only a sigkill gets the job done
			# and no, 'exit' does not work
			kill -9 $$
		" --content="
			$(timeLeft) is left for ${alarmTime}
		"
	# do not comment the next sleep command, otherwise
	# the loop will keep running continuously
	# and the repeated calls to `termux-notification`
	# may hog the resources of the device
	# Also, do not set `sleep` to pause less
	# than 60 seconds since `secondsLeft` refreshes
	# every 60 seconds (because `termux-dialog time` only outputs
	# HH:MM and not HH:MM:SS) anyway so it is meaningless
	# You may set it to pause more than 60 seconds
	# like 100 but you must also change the other 60 in this
	# while loop to 100 or more otherwise secondsLeft
	# may skip the period between 0 and 60 and the
	# alarm will be missed.
	# If you do not understand any of the above just leave this
	# part alone; 60 seconds is already ideal
	sleep 60
done
# the function that starts the alarm{{{1

ALARM() {

	# this loop will keep playing the ringtone
	# continously until the stop button is pressed
	# in the notification
	while true; do
		termux-media-player play "${ringtone}" >/dev/null
		sleep "${ringtonDuration-}"
	done &

	# we need the PID of the loop so that we can
	# kill it later (we have to, otherwise termux-media-player
	# will never stop)
	loopPid=$!

	termux-notification \
		--id="${notificationId}" \
		--ongoing \
		--icon alarm \
		-c 'stop the alarm?' \
		--button1 'stop' --button1-action "
		termux-notification-remove ${notificationId}
		termux-media-player stop

		# stop the background loop
		kill ${loopPid}
		"

	# $i will just be the number of seconds of time it took
	# in the next while loop (we will need it later)
	i=0

	# the loop will keep going till $loopPid is killed
	while [ -d "/proc/${loopPid}" ]; do
		i=$((i + 1))

		# raise the sound level to the maximum
		# once the first loop is done (25 corresponds
		# to the maximum volume step on LineageOS;
		# yours might be different)
		if [ "${i}" -eq "${ringtonDuration}" ]; then
			termux-volume music 25
		fi
		sleep 1
	done
}
# prevent oversleeping{{{1

# the alarm will start the first time
# once you stop it, it will pause for
# $ringtonDuration and it will ring again
# for $ringtonDuration but with low volume
# if you fail to stop it once and for all
# in that tight duration, it will
# resume but with full volume and will
# keep looping this way unless you
# get your shit together

# We will take advantage of global variables
i="${ringtonDuration}"
while [ "${i}" -ge "${ringtonDuration}" ]; do

	# make sure the sound is set to
	# the highest level
	termux-volume music 25

	# the first alarm
	ALARM

	# the pause in-between to test
	# whether you went back to sleep
	# after stopping the earlier alarm
	sleep "${ringtonDuration}"

	# set the sound level to a lower one
	termux-volume music 5

	# the second alarm: if you do not
	# stop it early the loop will continue
	ALARM
done

# vi:fdm=marker
