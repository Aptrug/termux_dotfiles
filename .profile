#!/bin/sh

# whatever commands are in these paths, they will override
# the commands in the original paths
export PATH="/data/data/com.termux/files/home/scripts/fajita:\
/data/data/com.termux/files/home/.local/share/node/bin:\
/data/data/com.termux/files/home/.cargo/bin:${PATH}"

# these paths will not override termux commands (like pactrag su)
export PATH="${PATH}:/data/data/com.termux/files/home/scripts/pactrag/no_arg:\
/data/data/com.termux/files/home/scripts/pactrag"

# must come after exporting PATH
(/data/data/com.termux/files/home/other_scripts/initialisation.sh >/dev/null 2>&1 &)

#aliases
alias pactrag_connect="ssh -C -4 -o CompressionLevel=9 -o UseDNS=no-o GSSAPIAuthentication=no -o 'PreferredAuthentications=publickey' -c aes128-ctr pactrag@192.168.1.174"
#alias refresh="apt-get update && apt-get autoremove -y && apt-get upgrade -y"
alias l="ls -alh"
alias xop="termux-clipboard-set"
# alias which="type -p"
alias which="command -v"
alias pox="/data/data/com.termux/files/home/scripts/fajita/getClipboard.sh"
#getclip
alias t="nvim /data/data/com.termux/files/home/misc/todo.txt"
alias ta="task add"
alias Download="cd /storage/emulated/0/Download"
alias Music="cd /storage/emulated/0/Music"
alias Movies="cd /storage/emulated/0/Movies"
alias 0="cd /storage/emulated/0"
alias Screenshots="cd /storage/emulated/0/Pictures/Screenshots"
alias Pictures="cd /storage/emulated/0/Pictures"
alias evie="sudo evillimiter -i wlan0 -g 192.168.2.1"
alias nvimSwapDelete='rm /data/data/com.termux/files/home/.local/share/nvim/swap/*'
# alias su="su -c 'bash'"
alias march="mpv /storage/emulated/0/Music/Preussen/*"
alias clear="clear; tmux clear-history; source /data/data/com.termux/files/home/.profile"
alias down="aria2c --no-conf"
alias fview="fzf --preview 'cat {}'"
alias gitload="git commit -a -m '‏‏‎' && git push -u origin master"
alias gitlogrm="git log --diff-filter=D --summary | grep delete"
alias tmp="cd /data/data/com.termux/files/usr/tmp"
alias gitrm="git rm --cached" # it actually `git rm --cached "$1"`
alias mpvn="mpv --no-resume-playback --save-position-on-quit=no"
alias net_scan="fping -a -g 192.168.1.101 192.168.1.146 | tr '\n' ' '"
alias netspeed="curl -L -o /dev/null http://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-KDE-Live-x86_64-Current.iso"
alias rmempty="find . -maxdepth 1 -empty -delete"
alias xon="perl -e 'ioctl STDOUT, 0x5412, \$_ for split //, do{ chomp(\$_ = <>); \$_ }; print \"\\n\"'"
alias yotu="youtube-viewer --region=DE --catlang=DE --audio --colorful"
alias termuxRefresh="am start -a com.llamalab.automate.intent.action.START_FLOW -d content://com.llamalab.automate.provider/flows/73/statements/1 &"
alias pointer_location="/data/data/com.termux/files/home/other_scripts/pointer_location.sh"
alias imgDisp="fzf --border=horizontal --preview 'viu -w 60 {}' --preview-window=up"

PS1='\[\e[0;32m\]\w\[\e[0m\]> '
export PAGER="nvim -R -n +Man!"
export MOST_INITFILE=/data/data/com.termux/files/home/.config/most/mostrc
export EDITOR=nvim
export VISUAL=nvim
export HISTFILE=/data/data/com.termux/files/home/.cache/bash/history

export XDG_DOCUMENTS_DIR=/storage/emulated/0/Documents
export XDG_DOWNLOAD_DIR=/storage/emulated/0/Download
export XDG_MUSIC_DIR=/storage/emulated/0/Music
export XDG_PICTURES_DIR=/storage/emulated/0/Pictures
export XDG_VIDEOS_DIR=/storage/emulated/0/Videos

# run tmux
[ -z "${TMUX-}" ] && exec /data/data/com.termux/files/usr/bin/tmux new -A -s fajita
