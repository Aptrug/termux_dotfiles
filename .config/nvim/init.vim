set undofile
set number
set tabstop=4 " a
set softtabstop=4 " b
set shiftwidth=4 " s
" both *case are needed
set ignorecase      " ignore case when searching
set smartcase       "
set splitbelow
set textwidth=80
set directory=./

nnoremap <CR> :noh<CR><CR>



" vim-surround diy
vnoremap <silent> S( xi()<esc>P
vnoremap <silent> Sb xi()<esc>P
vnoremap <silent> S[ xi[]<esc>P
vnoremap <silent> S{ xi{}<esc>P
vnoremap <silent> SB xi{}<esc>P
vnoremap <silent> S<lt> xi<lt>><esc>P
vnoremap <silent> S' xi''<esc>P
vnoremap <silent> S" xi""<esc>P

" spelling
" set spell spelllang=en_us

set clipboard+=unnamedplus

"  \      '+': '/data/data/com.termux/files/home/other_scripts/getClipboard.sh',
"  \      '*': '/data/data/com.termux/files/home/other_scripts/getClipboard.sh',

" let g:clipboard = {
" 	  \   'name': 'klip',
" 	  \   'copy': {
" 	  \      '+': 'termux-clipboard-set',
" 	  \      '*': 'termux-clipboard-set',
" 	  \    },
" 	  \   'paste': {
" 	  \      '+': 'termux-clipboard-get',
" 	  \      '*': 'termux-clipboard-get',
" 	  \   }
" 	  \ }

    let g:clipboard = {
          \   'name': 'myClipboard',
          \   'copy': {
          \      '+': 'tmux load-buffer -',
          \      '*': 'tmux load-buffer -',
          \    },
          \   'paste': {
          \      '+': 'tmux save-buffer -',
          \      '*': 'tmux save-buffer -',
          \   },
          \   'cache_enabled': 1,
          \ }

"augroup shell
"	autocmd Filetype sh setlocal equalprg=shfmt
"	autocmd FileType sh setlocal makeprg=/data/data/com.termux/files/usr/bin/bashate
"	autocmd InsertLeave *.sh silent make! <afile> | silent redraw!
"	"autocmd QuickFixCmdPost [^l]* cwindow
"augroup END

" nnoremap <C-p> :read! /data/data/com.termux/files/home/scripts/fajita/getClipboard.sh<CR>

"autocmd TextYankPost * call system('termux-clipboard-set', @")

nnoremap cc "_dd
vnoremap c "_d
vnoremap c "_d
nnoremap c "_d
nnoremap C "_D
" make remove actually remove
nnoremap x "_x
nnoremap X "_X
vnoremap x "_x
vnoremap X "_X
nnoremap s "_s
nnoremap S "_S
vnoremap s "_s
vnoremap S "_S

" Toggle the terminal
let g:term_buf = 0
let g:term_win = 0
function! Term_toggle(height)
    if win_gotoid(g:term_win)
        hide
    else
        botright new
        exec "resize " . a:height
        try
            exec "buffer " . g:term_buf
        catch
            call termopen($SHELL, {"detach": 0})
            let g:term_buf = bufnr("")
        endtry
        startinsert!
        let g:term_win = win_getid()
    endif
endfunction

nnoremap <C-q> :call Term_toggle(10)<cr>
tnoremap <C-q> <C-\><C-n>:call Term_toggle(10)<cr>


" comment toggle for certain file types
autocmd FileType c,cpp,java let b:comment_leader = '//'
autocmd FileType sh,ruby,python   let b:comment_leader = '#'
autocmd FileType conf,fstab       let b:comment_leader = '#'
autocmd FileType tex              let b:comment_leader = '%'
autocmd FileType mail             let b:comment_leader = '>'
autocmd FileType vim              let b:comment_leader = '"'
function! CommentToggle()
    execute ':silent! s/\([^ ]\)/' . escape(b:comment_leader,'\/') . ' \1/'
    execute ':silent! s/^\( *\)' . escape(b:comment_leader,'\/') . ' \?' . escape(b:comment_leader,'\/') . ' \?/\1/'
endfunction
nnoremap <silent> <C-c> :call CommentToggle()<CR>

" highlight changed lines
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
          \ | wincmd p | diffthis
endif
" Load Once: {{{1
augroup rememberCursorPosition
    autocmd!
    autocmd BufReadPost *
        \ if line("'\"") > 1 && line("'\"") <= line("$") && &ft !~# 'commit'
        \ | execute "normal! g`\"zvzz"
        \ | endif
augroup END
" }}}

function! RemoveTrailingSpaces()
	let l:winview = winsaveview()
  	" without the last `e` in the this specific regex, Neovim will complain
  	" if it has not found any matching patterns, that would be annoying
 	%s/\s\+$\|\_s*\%$//e
	call winrestview(l:winview)
endfunction

augroup removeAllTrailingWhitespaces
 	autocmd BufWritePre * call RemoveTrailingSpaces()
augroup end

nmap <S-Enter> O<Esc>j nmap <CR> o<Esc>k

" "! inserts shell command output into paste buffer
fu! SetBang(v) range
        if a:v == 1
                normal gv
        endif
        let l:t = &shellredir
        let &shellredir = ">%s\ 2>/dev/tty"
        let @" = join(systemlist(input("\"!"))," ")
        let &shellredir = l:t
endf
nnoremap "! :cal SetBang(0)<cr>
xnoremap "! :cal SetBang(1)<cr>

nnoremap gev :tabnew $MYVIMRC<CR>
nnoremap gsv :source $MYVIMRC<CR>

" autosave function (call it when you need it)
function! SaveAsYouType()
	autocmd TextChanged,TextChangedI <buffer> silent write
endfunction

" Make netrw open files in the same window (similar to NerdTree).
let g:netrw_browse_split = 4
