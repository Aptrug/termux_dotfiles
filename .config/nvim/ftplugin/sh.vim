setlocal equalprg=awk\ --file\ -\ --pretty-print=-
setlocal makeprg=shellcheck\ %
" setlocal formatprg=shellharden\ --transform

function! AutoFormat()
		" setlocal equalprg=shellharden\ --transform\ /dev/stdin
		setlocal equalprg=shfmt\ -i\ 0
		let l:winview = winsaveview()
		silent normal! gg=G
		call winrestview(l:winview)
		" silent normal! zz
		setlocal equalprg=awk\ --file\ -\ --pretty-print=-
endfunction

augroup shsyntaxcheck
  autocmd BufWrite <buffer> call AutoFormat()
augroup END

augroup LintOnSave
  autocmd! BufWritePost,BufEnter <buffer> silent make
  " autocmd BufWritePost * silent make! <afile> | silent redraw!
augroup END

augroup show_cwindow_on_save
	autocmd QuickFixCmdPost [^l]* cwindow
augroup END

" AWK Embedding:
" ==============
" Shamelessly ripped from aspperl.vim by Aaron Hope.
if exists("b:current_syntax")
  unlet b:current_syntax
endif
syn include @AWKScript syntax/awk.vim
syn region AWKScriptCode matchgroup=AWKCommand start=+[=\\]\@<!'+ skip=+\\'+ end=+'+ contains=@AWKScript contained
syn region AWKScriptEmbedded matchgroup=AWKCommand start=+\<gawk\>+ skip=+\\$+ end=+[=\\]\@<!'+me=e-1 contains=@shIdList,@shExprList2 nextgroup=AWKScriptCode
syn cluster shCommandSubList add=AWKScriptEmbedded
hi def link AWKCommand Type
