(defvar server-socket-dir
  (and (featurep 'make-network-process '(:family local))
   (format "%s/emacs%d" (or (getenv "TMPDIR") "$TMPDIR") (user-uid)))
  "The directory in which to place the server socket.
  If local sockets are not supported, this is nil.")

(require 'package)
(add-to-list 'package-archives
	     '("MELPA Stable" . "https://melpa.org/packages/") t)
(package-initialize)
(if (not (package-installed-p 'use-package))
    (progn
      (package-refresh-contents)
      (package-install 'use-package)))
(require 'use-package)

(use-package polymode
  :ensure t
  :defer t
  :mode ("\\.sh$" . poly-shell-perl-mode)
  :config (define-hostmode poly-shell-hostmode
            :mode 'shell-mode)
  (define-innermode poly-perl-shell-innermode
    :mode 'perl-mode
    :head-matcher "'''"
    :tail-matcher "'''"
    :head-mode 'host
    :tail-mode 'host)
  (define-polymode
    poly-shell-perl-mode
    :hostmode 'poly-shell-hostmode
    :innermodes '(poly-perl-shell-innermode)))

(message "Loaded init-polymode.el")
(provide 'init-polymode)

(use-package nix-mode
  :ensure t
  :mode "\\.nix\\'")

;;--------------------------------------------------------------------------------
;; store all backup and autosave files somewhere in /tmp
(setq backup-directory-alist
      `((".*" . , "/data/data/com.termux/files/home/.cache/emacs")))
(setq auto-save-file-name-transforms
      `((".*" , "/data/data/com.termux/files/home/.cache/emacs" t)))

;; show changed lines
;;(global-display-line-numbers-mode t)

;; eshell aliases
(defalias 'open 'find-file)

;; show both global and relative line numbers
(defun linum-relative-right-set-margin ()
  "Make width of right margin the same as left margin"
  (let* ((win (get-buffer-window))
     (width (car (window-margins win))))
    (set-window-margins win width width)))

(defadvice linum-update-current (after linum-left-right-update activate)
  "Advice to run right margin update"
  (linum-relative-right-set-margin)
  (linum-relative-right-update (line-number-at-pos)))

(defadvice linum-delete-overlays (after linum-relative-right-delete activate)
  "Set margins width to 0"
  (set-window-margins (get-buffer-window) 0 0))

(defun linum-relative-right-update (line)
  "Put relative numbers to the right margin"
  (dolist (ov (overlays-in (window-start) (window-end)))
    (let ((str (overlay-get ov 'linum-str)))
      (if str
      (let ((nstr (number-to-string
               (abs (- (string-to-number str) line)))))
        ;; copy string properties
        (set-text-properties 0 (length nstr) (text-properties-at 0 str) nstr)
        (overlay-put ov 'after-string
             (propertize " " 'display `((margin right-margin) ,nstr))))))))

;;------------good-emacs-default-----------------------
;; show matching parentheses
(show-paren-mode 1)

;; delete trailing lines and trailing whitespaces on save
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(save-place-mode)               ;; remember point position when saving file

(setq visible-bell t)			; Make beeps visible

(setq dired-listing-switches "-alh")	; ls -alh for dired (Emacs file manager)

;;------------disabling-emacs-shit-defaults-------------
;; disable welcome message
(setq inhibit-splash-screen t)
;; stop emacs from adding shit to init.el without permission
(setq custom-file "~/.cache/emacs/custom.el")
(load custom-file)
;; no more typing the whole yes or no. Just y or n will do.
(fset 'yes-or-no-p 'y-or-n-p)
;; scroll one line at the time please
;(setq scroll-conservatively most-positive-fixnum)

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time


;;disable splash screen and startup message
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)

;; enable dark mode (tango-dark is the least ugliest
;; builtin theme)
(load-theme 'tango-dark t)


;;-----------------toggle-terminal---------------------

(defun my/ansi-term-toggle ()
  "Toggle ansi-term window on and off with the same command."
  (interactive)
  (defvar my--ansi-term-name "ansi-term-popup")
  (defvar my--window-name (concat "*" my--ansi-term-name "*"))
  (cond ((get-buffer-window my--window-name)
         (ignore-errors (delete-window
                         (get-buffer-window my--window-name))))
        (t (split-window-right)
           (other-window 1)
           (cond ((get-buffer my--window-name)
                  (switch-to-buffer my--window-name))
                 (t (ansi-term "bash" my--ansi-term-name))))))
(global-set-key (kbd "<f12>") 'my/ansi-term-toggle)

;; running `exit` while in ansi-term actually makes you exit
(defadvice term-handle-exit
    (after term-kill-buffer-on-exit activate)
  ;; kill-buffer alone is not enough
  (kill-buffer-and-window))

;; this gets rid of "active processes exist; kill them and exit anyway"
;; for ansi-term only
(defun set-no-process-query-on-exit ()
  (let ((proc (get-buffer-process (current-buffer))))
    (when (processp proc)
      (set-process-query-on-exit-flag proc nil))))
(add-hook 'term-exec-hook 'set-no-process-query-on-exit)

;; Use header-line-format to mark the 80th column on the header.
(setq-default header-line-format
              (list " " (make-string 80 ?-) "|"))

;; assign shortcut for revert-buffer AKA :e!
(global-set-key [f5] 'revert-buffer-no-confirm)

;; 4 spaces tab instead of 8
(setq-default tab-width 4)

;; reload emacs configuration
(defun reload-init-file ()
  (interactive)
  (load-file "~/.config/emacs/init.el"))

;; Revert buffer without confirmation
(defun revert-buffer-no-confirm ()
  "Revert buffer without confirmation."
  (interactive)
  (revert-buffer t t))
(global-set-key (kbd "C-x r") 'revert-buffer-no-confirm)

;;-------------------------------------------------------------------------
;; %f is the absolute path while %b is the relative path
;; %c starts counting from 0, Vim uses %C, so I will use %C
;; %i shows file size in bytes, %I shows it in human readable sizes
;; %o seems to be better than %p, %P and %q
(setq-default mode-line-format (list "%* %f (%m) %I %C %o"))

;; open init.el in a new tab in order to quickly modify it
(global-set-key (kbd "C-c I")
				(lambda () (interactive) (tab-new)
				  (find-file "/data/data/com.termux/files/home/.config/emacs/init.el")))


;; stop emacs from warning me about a file being too big to open
(setq large-file-warning-threshold nil)
